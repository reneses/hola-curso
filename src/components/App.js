import React, { Component } from "react";
import styled from "@emotion/styled";
import background from "../assets/img/background.jpeg";
import Main from "./Main";
import { TextInputForm } from "./core";
import { loadName, persistName } from "../lib/storage";
import Weather from "./Weather";

class App extends Component {
  state = {
    name: loadName()
  };

  handleNameSubmit = name => {
    this.setState(
      {
        name
      },
      () => persistName(name)
    );
  };

  handleNameClear = () => this.handleNameSubmit(null);

  render() {
    const { className } = this.props;
    const { name } = this.state;

    return (
      <div className={className}>
        <Weather />
        {name ? (
          <Main name={name} onNameClear={this.handleNameClear} />
        ) : (
          <TextInputForm
            onSubmit={this.handleNameSubmit}
            placeholder="¿Cómo te llamas?"
          />
        )}
      </div>
    );
  }
}

export default styled(App)`
  height: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  background: black;
  background-size: cover;
  background-position: center;
  background-image: url('${background}');
  
  .Weather {
    position: absolute;
    top: 20px;
    right: 20px;
  }
`;
