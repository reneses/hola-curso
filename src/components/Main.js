import React, { Component } from "react";
import PropTypes from "prop-types";
import dayjs from "dayjs";
import { Button, Text } from "./core";

class Main extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onNameClear: PropTypes.func.isRequired
  };

  state = {
    time: this.time
  };

  interval = null;

  componentDidMount() {
    this.interval = setInterval(
      () =>
        this.setState(prevState => {
          const { time } = this;

          if (time !== prevState.time) {
            this.setState({
              time
            });
          }

          return null;
        }),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  get time() {
    return dayjs().format("HH:mm");
  }

  get greetings() {
    const hour = dayjs().hour();

    if (hour > 5 && hour < 13) {
      return "Buenos días";
    }

    if (hour >= 13 && hour < 21) {
      return "Buenas tardes";
    }

    return "Buenas noches";
  }

  render() {
    const { name, onNameClear } = this.props;
    const { time } = this.state;

    return (
      <main className="Main" style={{ textAlign: "center" }}>
        <Text style={{ fontSize: "6em" }}>{time}</Text>
        <Text style={{ fontSize: "2em" }}>
          {this.greetings},{" "}
          <Button underline onClick={onNameClear}>
            {name}
          </Button>
        </Text>
      </main>
    );
  }
}

export default Main;
