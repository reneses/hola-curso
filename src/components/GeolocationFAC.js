import { Component } from "react";
import PropTypes from "prop-types";

class GeolocationFAC extends Component {
  static displayName = "GeolocationFAC";
  static propTypes = {
    children: PropTypes.func.isRequired
  };

  state = {
    coordinates: null,
    hasLoaded: false
  };

  componentDidMount() {
    if (!navigator.geolocation) {
      return;
    }

    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        this.setState({
          coordinates: { latitude, longitude },
          hasLoaded: true
        });
      },
      error => {
        this.setState({
          coordinate: null,
          hasLoaded: true
        });
      }
    );
  }

  render() {
    const { children } = this.props;
    const { coordinates, hasLoaded } = this.state;

    return children({ coordinates, hasLoaded });
  }
}

export default GeolocationFAC;
