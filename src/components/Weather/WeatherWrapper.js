import React, { Component } from "react";
import {
  getWeatherByCity,
  getWeatherByCoordinates
} from "../../services/weather";
import UnitConverWeather from "./UnitConverterWeather";
import { TextInputForm } from "../core";
import { loadCity, persistCity } from "../../lib/storage";

class WeatherWrapper extends Component {
  state = {
    temperature: null,
    city: null,
    isLoading: false
  };

  componentDidMount() {
    const { coordinates } = this.props;
    this.handleCityChange(loadCity(), coordinates);
  }

  handleCityChange = (city = null, coordinates = null) => {
    if (!city && !coordinates) {
      this.setState(
        {
          temperature: null,
          city: null
        },
        () => persistCity(null)
      );
      return;
    }

    this.setState(
      {
        isLoading: true
      },
      () => {
        this.getWeather(city, coordinates)
          .then(result => {
            const { main, name } = result.data;
            const { temp } = main;

            this.setState(
              {
                temperature: temp,
                city: name
              },
              () => persistCity(name)
            );
          })
          .catch(err => {
            console.error(err);
          })
          .finally(() => {
            this.setState({ isLoading: false });
          });
      }
    );
  };

  getWeather = (city, coordinates) => {
    return coordinates
      ? getWeatherByCoordinates(coordinates)
      : getWeatherByCity(city);
  };

  handleCityClear = () => this.handleCityChange();

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return null;
    }

    return <div className="Weather">{this.renderContent()}</div>;
  }

  renderContent() {
    const { temperature, city } = this.state;

    if (!city) {
      return (
        <TextInputForm
          placeholder="¿Ciudad?"
          onSubmit={this.handleCityChange}
          style={{ fontSize: "1.4em", width: "150px" }}
        />
      );
    }

    return (
      <UnitConverWeather
        onCityClear={this.handleCityClear}
        temperatureKelvin={temperature}
        city={city}
      />
    );
  }
}

export default WeatherWrapper;
