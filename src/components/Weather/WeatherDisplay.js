import React from "react";
import PropTypes from "prop-types";
import { Button } from "../core";

const WeatherDisplay = ({
  temperature,
  city,
  unitSymbol,
  toggleTemperature,
  onCityClear
}) => (
  <div style={{ textAlign: "center" }}>
    <Button
      onClick={toggleTemperature}
      style={{ fontSize: "1.8em", display: "block", textAlign: "center" }}>
      {temperature}º {unitSymbol}
    </Button>
    <Button style={{ fontSize: "1.44em" }} onClick={onCityClear}>
      {city}
    </Button>
  </div>
);

WeatherDisplay.propTypes = {
  temperature: PropTypes.number.isRequired,
  city: PropTypes.string.isRequired,
  unitSymbol: PropTypes.string.isRequired,
  toggleTemperature: PropTypes.func.isRequired,
  onCityClear: PropTypes.func.isRequired
};

export default WeatherDisplay;
