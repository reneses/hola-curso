import React, { Component } from "react";
import PropTypes from "prop-types";
import WeatherDisplay from "./WeatherDisplay";
import { kelvinToCelsius, kelvinToFarenheit } from "../../lib/weather";
import { loadWeatherUnit, persistWeatherUnit } from "../../lib/storage";

class UnitConverWeather extends Component {
  static displayName = "UnitConverWeather";
  static propTypes = {
    temperatureKelvin: PropTypes.number.isRequired,
    city: PropTypes.string.isRequired,
    onCityClear: PropTypes.func.isRequired
  };

  state = {
    unitSymbol: loadWeatherUnit() || "C"
  };

  get temperature() {
    const { temperatureKelvin } = this.props;
    const { unitSymbol } = this.state;

    if (unitSymbol === "C") {
      return kelvinToCelsius(temperatureKelvin);
    }

    return kelvinToFarenheit(temperatureKelvin);
  }

  handleToggleTemperature = () => {
    this.setState(prevState => {
      const unitSymbol = prevState.unitSymbol === "C" ? "F" : "C";
      persistWeatherUnit(unitSymbol);

      return {
        unitSymbol
      };
    });
  };

  render() {
    const { city, onCityClear } = this.props;
    const { unitSymbol } = this.state;

    return (
      <WeatherDisplay
        city={city}
        temperature={this.temperature}
        unitSymbol={unitSymbol}
        toggleTemperature={this.handleToggleTemperature}
        onCityClear={onCityClear}
      />
    );
  }
}

export default UnitConverWeather;
