import React from "react";
import { shallow, mount } from "enzyme";
import WeatherDisplay from "../WeatherDisplay";
import { Button } from "../../core";

const toggleTemperature = jest.fn();
const onCityClear = jest.fn();

const baseProps = {
  temperature: 21,
  city: "Vigo",
  unitSymbol: "C",
  toggleTemperature,
  onCityClear
};

describe("<WeatherDisplay />", () => {
  describe("render", () => {
    let wrapper, buttons;

    beforeEach(() => {
      wrapper = shallow(<WeatherDisplay {...baseProps} />);
      buttons = wrapper.find(Button);
    });

    it("matches snapshot", () => {
      expect(wrapper).toMatchSnapshot();
    });

    it("renders two buttons", () => {
      expect(buttons).toHaveLength(2);
    });

    it("renders the temperature and unit symbol in the first button", () => {
      expect(buttons.at(0).text()).toContain(
        `${baseProps.temperature}º ${baseProps.unitSymbol}`
      );
    });

    it("renders city in the second button", () => {
      expect(buttons.at(1).text()).toBe(baseProps.city);
    });
  });

  describe("callbacks", () => {
    let wrapper;
    let buttons;

    beforeEach(() => {
      jest.resetAllMocks();
      wrapper = mount(<WeatherDisplay {...baseProps} />);
      buttons = wrapper.find(Button);
    });

    describe("when clicking the first button", () => {
      it("triggers toggleTemperature", () => {
        buttons.at(0).simulate("click");
        expect(toggleTemperature).toHaveBeenCalledTimes(1);
        expect(onCityClear).toHaveBeenCalledTimes(0);
      });
    });

    describe("when clicking the second button", () => {
      it("triggers onCityClear", () => {
        buttons.at(1).simulate("click");
        expect(toggleTemperature).toHaveBeenCalledTimes(0);
        expect(onCityClear).toHaveBeenCalledTimes(1);
      });
    });
  });
});
