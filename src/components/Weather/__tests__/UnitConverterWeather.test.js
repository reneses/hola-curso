import React from "react";
import { shallow } from "enzyme";
import UnitConverWeather from "../UnitConverterWeather";
import WeatherDisplay from "../WeatherDisplay";
import { kelvinToCelsius, kelvinToFarenheit } from "../../../lib/weather";

const baseProps = {
  temperatureKelvin: 100,
  city: "Vigo",
  onCityClear: jest.fn()
};

describe("<UnitConverterWeather />", () => {
  let wrapper, weatherDisplayWrapper;

  beforeEach(() => {
    wrapper = shallow(<UnitConverWeather {...baseProps} />);
    weatherDisplayWrapper = wrapper.find(WeatherDisplay);
  });

  it("matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("propagates the city prop", () => {
    expect(weatherDisplayWrapper.prop("city")).toBe(baseProps.city);
  });

  it("propagates the onCityClear prop", () => {
    expect(weatherDisplayWrapper.prop("onCityClear")).toBe(
      baseProps.onCityClear
    );
  });

  describe("temperature conversion", () => {
    it("defaults to celisus", () => {
      expect(weatherDisplayWrapper.prop("unitSymbol")).toBe("C");
      expect(weatherDisplayWrapper.prop("temperature")).toBe(
        kelvinToCelsius(baseProps.temperatureKelvin)
      );
    });

    it("toggles to farenheit when clicking", () => {
      weatherDisplayWrapper.prop("toggleTemperature")();

      wrapper.update();
      weatherDisplayWrapper = wrapper.find(WeatherDisplay);

      expect(weatherDisplayWrapper.prop("unitSymbol")).toBe("F");
      expect(weatherDisplayWrapper.prop("temperature")).toBe(
        kelvinToFarenheit(baseProps.temperatureKelvin)
      );
    });
  });
});
