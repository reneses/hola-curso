import React from "react";
import WeatherWrapper from "./WeatherWrapper";
import GeolocationFAC from "../GeolocationFAC";

const WeatherWithGeolocation = () => (
  <GeolocationFAC>
    {({ coordinates, hasLoaded }) =>
      hasLoaded ? <WeatherWrapper coordinates={coordinates} /> : null
    }
  </GeolocationFAC>
);

export default WeatherWithGeolocation;
