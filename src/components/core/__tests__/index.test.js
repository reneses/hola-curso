import React from "react";
import { mount } from "enzyme";
import { Button, Text } from "../../core";

describe("<Text />", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(<Text>Some Text</Text>);
  });

  it("matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe("<Button />", () => {
  describe("render", () => {
    describe("default", () => {
      it("matches snapshot", () => {
        const wrapper = mount(<Button>Some Text</Button>);
        expect(wrapper).toMatchSnapshot();
      });
    });

    describe("with underline", () => {
      it("matches snapshot", () => {
        const wrapper = mount(<Button underline>Some Text</Button>);
        expect(wrapper).toMatchSnapshot();
      });
    });
  });

  it("propagates props", () => {
    const name = "hola";
    const wrapper = mount(<Button name={name}>Some Text</Button>);
    expect(wrapper.prop("name")).toBe(name);
  });

  it("overrides styles", () => {
    const fontSize = "1000em";
    const wrapper = mount(<Button style={{ fontSize }}>Some Text</Button>);
    expect(wrapper.prop("style").fontSize).toBe(fontSize);
  });
});
