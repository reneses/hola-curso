import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";

const StyledInput = styled("input")`
  background: transparent;
  border: 0;
  outline: none;
  font-size: 2em;
  text-align: center;
  border-bottom: 2px solid white;

  &,
  &::placeholder {
    color: white;
    text-shadow: 4px 4px 10px black;
    opacity: 1;
  }
`;

class TextInputForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    style: PropTypes.object
  };

  state = {
    value: null
  };

  handleChange = evt => {
    const { value } = evt.target;
    this.setState({
      value
    });
  };

  handleSubmit = evt => {
    evt.preventDefault();

    const { onSubmit } = this.props;
    const { value } = this.state;

    if (value) {
      onSubmit(value);
      return;
    }
  };

  render() {
    const { style, placeholder } = this.props;

    return (
      <form onSubmit={this.handleSubmit} className="TextInputForm">
        <StyledInput
          type="text"
          placeholder={placeholder}
          style={style}
          onChange={this.handleChange}
        />
      </form>
    );
  }
}

export default TextInputForm;
