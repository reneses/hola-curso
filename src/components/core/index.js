import styled from "@emotion/styled";

export { default as TextInputForm } from "./TextInputForm";

export const Text = styled("div")`
  color: white;
  text-shadow: 4px 4px 10px black;
`;

export const Button = styled("button")`
  color: white;
  text-shadow: 4px 4px 10px black;
  background: transparent;
  border: 0;
  cursor: pointer;
  outline: none;
  font-size: 1em;
  ${props => (props.underline ? "border-bottom: 2px solid white;" : "")}
`;
