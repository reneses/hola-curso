import axios from "axios";

const API_URL = "https://api.openweathermap.org/data/2.5";
const API_KEY = process.env.REACT_APP_WEATHER_API_KEY;

export const getWeatherByCity = city =>
  axios.get(`${API_URL}/weather?q=${city}&APPID=${API_KEY}`);

export const getWeatherByCoordinates = ({ longitude, latitude }) =>
  axios.get(
    `${API_URL}/weather?lat=${latitude}&lon=${longitude}&APPID=${API_KEY}`
  );
