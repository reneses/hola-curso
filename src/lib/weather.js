export const kelvinToCelsius = kelvin => Math.round(kelvin - 273.15);

export const kelvinToFarenheit = kelvin =>
  Math.round(kelvin * (9 / 5) - 459.67);
