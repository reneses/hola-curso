export const persist = key => value =>
  localStorage.setItem(key, JSON.stringify(value));

export const load = key => () => {
  try {
    return JSON.parse(localStorage.getItem(key)) || null;
  } catch (err) {
    return null;
  }
};

export const persistName = persist("name");
export const loadName = load("name");

export const persistWeatherUnit = persist("weatherUnit");
export const loadWeatherUnit = load("weatherUnit");

export const persistCity = persist("city");
export const loadCity = load("city");
