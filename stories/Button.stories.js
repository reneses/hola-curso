import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Button } from "../src/components/core";
import { greyBackground } from "./lib/decorators";
import { withKnobs, boolean, text, object } from "@storybook/addon-knobs";

const generateProps = ({ underline = false, style = {} } = {}) => ({
  onClick: action("onClick"),
  underline: boolean("Underline", underline),
  style: object("Styles", style)
});

storiesOf("Button", module)
  .addDecorator(withKnobs)
  .addDecorator(greyBackground)
  .addWithJSX("default", () => (
    <Button {...generateProps()}>{text("Text", "Texto del boton")}</Button>
  ))
  .addWithJSX("underlined", () => (
    <Button {...generateProps({ underline: true })}>Some text</Button>
  ))
  .addWithJSX("with style", () => (
    <Button {...generateProps({ style: { fontSize: "2em", color: "purple" } })}>
      Some text
    </Button>
  ));
