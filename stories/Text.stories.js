import React from "react";
import { storiesOf } from "@storybook/react";
import { Text } from "../src/components/core";
import { greyBackground } from "./lib/decorators";

storiesOf("Text", module)
  .addDecorator(greyBackground)
  .addWithJSX("default", () => <Text>Hello </Text>);
