import React from "react";

export const greyBackground = story => (
  <div
    style={{
      background: "grey",
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    }}>
    {story()}
  </div>
);
